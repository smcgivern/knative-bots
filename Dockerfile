FROM ruby:2.5

# Install production dependencies.
WORKDIR /usr/src/app
COPY Gemfile Gemfile.lock ./
ENV BUNDLE_FROZEN=true
RUN bundle install

# Copy local code to the container image.
COPY . .

# Configure and document the service HTTP port.
ENV PORT 8080
EXPOSE $PORT

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "8080"]
